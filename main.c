/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
/** @file
*
* @defgroup nrf_radio_test_example_main main.c
* @{
* @ingroup nrf_radio_test_example
* @brief Radio Test Example Application main file.
*
* This file contains the source code for a sample application using the NRF_RADIO, and is controlled through the serial port.
*
*/


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "bsp.h"
#include "nrf.h"
#include "radio_test.h"
#include "app_uart.h"
#include "app_error.h"
#include "nordic_common.h"
#include "nrf_drv_gpiote.h"

#define LED_RED                                                  8
#define LED_GREEN                                                9
#define LED_BLUE                                                 10
#define LED_ON                           0                                          //Output state to turn LED on
#define LED_OFF                          1                                          //Output state to turn LED off

#define BTN_SENSE                                          6                                          /** Pin for sensing button status **/
#define SHTDWN                                           7                                          /** Pin for controlling power transistors  **/
#define DEVICE_OFF                       0                                          //Output state to turn device off with SHTDWN pin
#define DEVICE_ON                        1                                          //Output state to turn device on with SHTDWN pin
#define BATTERY_MEASURE                                  4                                          /** Pin used by the ADC to measure battery status, defined by the voltage divided in half by two resistors. **/
#define BATTERY_CHARGING                                 2                                          /** Status pin of the battery charger IC indicating charging staus**/


static uint8_t mode_          = RADIO_MODE_MODE_Nrf_1Mbit;
static uint8_t txpower_       = RADIO_TXPOWER_TXPOWER_0dBm;
static int channel_start_     = 2;
static int channel_high_       = 80;
static int channel_mid_      = 40;
static int delayms_           = 10;

static bool sweep = false;

typedef enum
{
    RADIO_TEST_NOP = 0,      /**< No test running.      */
    RADIO_TEST_TXCC_LOW,     /**< TX constant carrier.  */
    RADIO_TEST_TXCC_MID,     /**< TX constant carrier.  */
    RADIO_TEST_TXCC_HIGH,     /**< TX constant carrier.  */
    RADIO_TEST_TXMC_LOW,     /**< TX modulated carrier. */
    RADIO_TEST_TXMC_MID,     /**< TX modulated carrier. */
    RADIO_TEST_TXMC_HIGH,     /**< TX modulated carrier. */
    RADIO_TEST_RXC,      /**< RX constant carrier.  */
    RADIO_TEST_DEVICE_OFF  //turn device off
} radio_tests_t;

typedef struct
{
    uint8_t led_red_state;
    uint8_t led_green_state;
    uint8_t led_blue_state;
} radio_test_colors_t;

static radio_test_colors_t radio_test_colors[] = {
    {LED_ON, LED_ON, LED_ON}, //NOP white
    {LED_ON, LED_OFF, LED_OFF}, //Red Constant Carrier low channel
    {LED_OFF, LED_ON, LED_OFF}, //Green Constant Carrier mid channel
    {LED_OFF, LED_OFF, LED_ON}, //Blue Constant Carrier high channel
    {LED_ON, LED_ON, LED_OFF}, //Yellow Modulated Carrier low channel
    {LED_ON, LED_OFF, LED_ON}, //Magenta Modulated Carrier mid channel
    {LED_OFF, LED_ON, LED_ON}, //Cyan Modulated Carrier high channel
    {LED_ON, LED_ON, LED_ON}, //White RX mid channel
    {LED_ON, LED_ON, LED_ON}, //OFF will get shut off
};

static radio_tests_t test_mode = RADIO_TEST_NOP;

/**@brief Function for handling the button press
 */
void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action){
    test_mode++;
    switch (test_mode)
    {
        case RADIO_TEST_TXCC_LOW:
            radio_tx_carrier(txpower_, mode_, channel_start_);
            break;
        case RADIO_TEST_TXCC_MID:
            radio_tx_carrier(txpower_, mode_, channel_mid_);
            break;
        case RADIO_TEST_TXCC_HIGH:
            radio_tx_carrier(txpower_, mode_, channel_high_);
            break;
        case RADIO_TEST_TXMC_LOW:
             radio_modulated_tx_carrier(txpower_, mode_, channel_start_);
            break;
        case RADIO_TEST_TXMC_MID:
             radio_modulated_tx_carrier(txpower_, mode_, channel_mid_);
            break;
        case RADIO_TEST_TXMC_HIGH:
             radio_modulated_tx_carrier(txpower_, mode_, channel_high_);
            break;
        case RADIO_TEST_RXC:
            radio_rx_carrier(mode_, channel_start_);
            break;
        case RADIO_TEST_DEVICE_OFF:
            nrf_gpio_pin_write(SHTDWN, DEVICE_OFF); //Turn off the device 
            break;
        default:
            nrf_gpio_pin_write(SHTDWN, DEVICE_OFF); //Turn off the device 
            break;
    }
    //set leds
    nrf_gpio_pin_write(LED_GREEN, radio_test_colors[test_mode].led_green_state);
    nrf_gpio_pin_write(LED_RED, radio_test_colors[test_mode].led_red_state);
    nrf_gpio_pin_write(LED_BLUE, radio_test_colors[test_mode].led_blue_state);
}

/** @brief Function for configuring all peripherals used in this example.
*/
static void init(void)
{
    NRF_RNG->TASKS_START = 1;

    // Start 16 MHz crystal oscillator
    NRF_CLOCK->EVENTS_HFCLKSTARTED  = 0;
    NRF_CLOCK->TASKS_HFCLKSTART     = 1;

    // Wait for the external oscillator to start up
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
        // Do nothing.
    }

    //initialize i/o
   uint32_t err_code = nrf_drv_gpiote_init(); //Initialize the general purpose io hardware
   APP_ERROR_CHECK(err_code);

    //The logic for setting up button pressing and power handling
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true); // Detect a falling edge
    in_config.pull = NRF_GPIO_PIN_PULLUP; // Setup a pullup to avoid a floating pin
    nrf_gpio_cfg_input(BTN_SENSE, NRF_GPIO_PIN_PULLUP); // This is necessary for sensing button state to turn the device off
    err_code = nrf_drv_gpiote_in_init(BTN_SENSE, &in_config, in_pin_handler); // initialize gpio pin
    APP_ERROR_CHECK(err_code); // Check for errors
    nrf_drv_gpiote_in_event_enable(BTN_SENSE, true); //Enable the interrupt for handling button presses

    nrf_gpio_cfg_output(SHTDWN); // Set the shutdown pin as an ouput
    nrf_gpio_pin_write(SHTDWN, DEVICE_ON); // Pull up the power line as soon as the application boots up to keep the device on

    nrf_gpio_cfg_input(BATTERY_CHARGING, NRF_GPIO_PIN_PULLUP);

    nrf_gpio_cfg_output(LED_BLUE);
    nrf_gpio_cfg_output(LED_GREEN);
    nrf_gpio_cfg_output(LED_RED);

    //turn white to show on - not doing anything
    nrf_gpio_pin_write(LED_GREEN, LED_ON);
    nrf_gpio_pin_write(LED_RED, LED_ON);
    nrf_gpio_pin_write(LED_BLUE, LED_ON);

}


/** @brief Function for main application entry.
 */
int main(void)
{
    uint32_t err_code;

    init();

    NVIC_EnableIRQ(TIMER0_IRQn);
    __enable_irq();
    while (true)
    {
    }
}

/** @} */

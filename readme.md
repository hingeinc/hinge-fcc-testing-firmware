Firmware to provide radio output for testing for FCC certification

Built with Segger Embedded Studio

Depends on Nordic SDK 14.2.0_17b948a being located here relative to project: ../../nRF5_SDK_14.2.0_17b948a/

After power on the button cycles through radio test modes and led indicates mode as follows:
	1) White - Radio off (at power up)
	2) Red - Constant Carrier low channel 
	3) Green - Constant Carrier mid channel
	4) Blue - Constant Carrier high channel
	5) Yellow - Modulated Carrier low channel
	6) Magenta - Modulated Carrier mid channel
	7) Cyan - Modulated Carrier high channel
	8) White - RX mid channel
	9) Device off

Power is set at 0dbm for all tests.  Data rate 1Mb/s for all test.